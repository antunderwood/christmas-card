function drawTree() {
  const {width, height} = document.querySelector("main").getBoundingClientRect();
  const tree = new phylocanvas.PhylocanvasGL(
    document.querySelector("main"),
    {
      size: {width: width, height: height - 50},
      interactive: true,
      nodeSize: 60,
      type: "cr",
      showShapeBorders: false,
      fontColour: "red",
      fontSize:14 ,
      fillColour: "transparent",
      strokeColour: "DarkGreen",
      fontFamily: "'Gill Sans', sans-serif",
      zoom: 0.5,
      lineWidth: 4,
      lineWitdth: 4,
      showLabels: true,
      showLeafLabels: true,


      source: "(0:0.5000000000000001,1:0.5618018987172387,2:0.40081119539534427,3:0.38938412895876284,4:0.49328265968240836,5:0.6830127018922191,6:0.85065080835204,7:0.7472382749323044,8:0.6728163648031881,9:0.6180339887498949,10:0.5773502691896258,11:0.5473181392530234,12:0.5393446629166315,13:0.766755446148772,14:0.7541312096726374,15:0.75,16:0.7541312096726374,17:0.7667554461487719,18:0.5393446629166317,19:0.5473181392530233,20:0.5773502691896256,21:0.6180339887498948,22:0.6728163648031878,23:0.7472382749323045,24:0.8506508083520398,25:0.6830127018922191,26:0.49328265968240814,27:0.3893841289587629,28:0.4008111953953443,29:0.5618018987172387,30:0.5000000000000001,31:0.45493820739985835,32:0.4215640850011099,33:0.39680224666742053,34:0.37870694431738233,35:0.36602540378443865,36:0.35796047807979386,37:0.5310578822608587,38:0.5310578822608586,39:0.5369407171196907,40:0.5490381056766579,41:0.5680604164760734,42:0.5952033700011308,43:0.6323461275016645,44:0.6824073110997871,45:0.7499999999999999,46:0.6824073110997874,47:0.6323461275016646,48:0.595203370001131,49:0.5680604164760734,50:0.549038105676658,51:0.5369407171196908,52:0.5310578822608586,53:0.5310578822608587,54:0.3579604780797938,55:0.3660254037844386,56:0.3787069443173823,57:0.39680224666742053,58:0.42156408500110965,59:0.45493820739985813):0;",
    
    },
  );

  const globes = 32;
  const rays = 60;
  const colours = [ "FireBrick", "DarkOrange", "LightBlue", "MediumOrchid" ];
  const message = ["H","A","P","P","Y","","C","H","R","I","S","T","M","A","S","","A","N","D","","A", "", "H","E","A","L","T","H","Y","","2","0","2","1"]
  let start_pos = 0;
  tree.generateStyles = function() {
    start_pos = start_pos + 1; 

    const styles = {};


    // set labels
    for (let i = 0; i < rays; i++) {
      styles[i.toString()] = {label: ""};
    }
  
    styles["14"]["shape"] = "square"
    styles["14"]["fillColour"] = "SaddleBrown";
    styles["15"] = styles["14"];
    styles["16"] = styles["14"];
    
    
    styles["45"] = {
      shape: "star",
      fillColour: "GoldenRod",
      label: ""
    };
    if (start_pos % rays == 12){
      start_pos = 17
    }
    if (start_pos % rays == 44){
      start_pos = 45
    }
    let adjusted_pos = start_pos;
    for (let i = 0; i < message.length; i++){
      adjusted_pos = adjusted_pos + 1;
      adjusted_pos = adjusted_pos % rays;
      if (adjusted_pos == 13){ 
        adjusted_pos = 18;
      }
      if (adjusted_pos == 45) {
        adjusted_pos = 46;
      }
      
      styles[adjusted_pos.toString()]["label"] =  message[i];

    }

    for (let i = 0; i < globes; i++) {
      const index = Math.floor(Math.random() * rays);
      if (index < 13 || index > 17) {
        if (!(index > (45 - 2) && index < (45 + 2))) {
          styles[index.toString()]["shape"] = "dot";
          styles[index.toString()]["fillColour"] = colours[Math.floor(Math.random() * colours.length)];
        }
      }
    }

    tree.setState({ styles });
  }

  tree.generateStyles(start_pos);
  tree.fitInPanel();

  window.setInterval(
    tree.generateStyles,
    1 * 500,
  );
  window.addEventListener(
    "resize", 
    () => tree.setState({ size: document.querySelector("main").getBoundingClientRect() })
  );

}
